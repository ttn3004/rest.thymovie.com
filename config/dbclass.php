<?php
require_once 'meta.php';

class DBClass
{

    public $connection;

    // Create connection
    function getConnection()
    {
        $this->connection = null;
        $this->connection = new mysqli(SERVERNAME, USERNAME, PASSWORD, DBNAME);
        // Check connection
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }

        return $this->connection;
    }
}

?>