drop table TEMP_FILM;
CREATE TABLE TEMP_FILM (
  ID int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  IDCINE int(18) NOT NULL unique,
  TITRE varchar(200),
  DATE_SORTIE varchar(100),
  LONGUEUR varchar(100),
  REALISATEUR varchar(500),
  ACTEURS varchar(500),
  GENRES varchar(200),	
  NATIONALITE varchar(100),
  SYNOPSIS varchar(2000),
  PROPERTIES varchar(800)	,
  IMAGE_PR varchar(100),
);
CREATE TABLE TEMP_SERIE (
  ID int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
  IDCINE int(18) NOT NULL unique,
  TITRE varchar(200),
  DATE_SORTIE varchar(100),
  LONGUEUR varchar(100),
  REALISATEUR varchar(500),
  ACTEURS varchar(500),
  GENRES varchar(200),	
  NATIONALITE varchar(100),
  SYNOPSIS varchar(2000),
  PROPERTIES varchar(800)	,
  NB_EPISODES int(4),
  NB_SEASONS int(4),
  IMAGE_PR varchar(100)
);

select * 
from film f, film_director fd , director d
where f.id=fd.film_id
and d.id=fd.director_id
and d.name like 'steven spi%'

select count(1) from SI where upper(NAME)='John Ford'

select * from film_distributor
select * from film_director
select * from film_ACTOR
select * from film_GENRE
select * from film_COUNTRY

delete from film;
delete from film_distributor;
delete from film_director;
delete from film_ACTOR;
delete from film_GENRE;
delete from film_COUNTRY;
ALTER TABLE film AUTO_INCREMENT = 1;
 
SELECT TABLE_NAME,COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_SCHEMA = 'thymovie' and TABLE_NAME='film'
 
select film.*,film_type.name
from film   join film_type on film.film_type_id=film_type.id
left join film_menu  on film_menu.film_id=film.id
left join menu on film_menu.menu_id=menu.id
left join film_country on film_country.film_id=film.id
left join country on film_country.country_id=country.id
left join film_genre on film_genre.film_id=film.id
left join genre on film_genre.genre_id=genre.id
where film.id=1

    "genre" => "foo",
    "country" => "foo",
    "film_type" => "foo",
    "director" => "foo",
    "productor" => "foo",
    "distributor" => "foo",
    "actor" => "foo",
    "keyword" => "foo",
    
select film.*,film_type.NAME as FILM_TYPE_NAME , film_images.IMAGES_PATH as IMAGES_PATH 
 from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id 
 where film_images.id  = (select max(id) from film_images where film_id=film.id )  and film.id in  
/*(select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id=1)*/
/*(select film_id from film_country join country on country.id=film_country.country_id and country.id=1)
/*(select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id=1)*/
/*(select film_id from film_director join director on director.id=film_director.director_id and director.id=1)*/
/*(select film_id from film_productor join productor on productor.id=film_productor.productor_id and productor.id=1)*/
/*(select film_id from film_distributor join distributor on distributor.id=film_distributor.distributor_id and distributor.id=1)	*/
/*(select film_id from film_actor join actor on actor.id=film_actor.actor_id and actor.id=1)*/
(select id from film WHERE MATCH(title,synopsis,original_title) AGAINST ('combat') > 0 ORDER BY MATCH(title,synopsis,original_title) AGAINST ('combat') DESC)


 select  * from productor;
  select  * from film_distributor;
  
select film.*,film_type.name    
from film   join film_type on film.film_type_id=film_type.id    
where film.id in  (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id = ?) limit 100 

select film.*,film_type.NAME    from film   join film_type on film.film_type_id=film_type.id    
where film.id in  (
select id from film WHERE MATCH(title,synopsis,original_title) AGAINST ('combat') > 0 ORDER BY MATCH(title,synopsis,original_title) AGAINST ('combat') DESC
)  limit 5;

alter table spectator add CREATED_AT timestamp DEFAULT CURRENT_TIMESTAMP

--delete from temp_serie
--DELETE from film where film_type_id=12
select * from temp_serie limit 10;

  
(select 'BESTS', film.*,film_type.NAME as FILM_TYPE_NAME,   film_images.IMAGES_PATH as IMAGES_PATH
 from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id
  where film_images.id = (select max(id) from film_images where film_id=film.id )
  and film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =1) limit 5)
  union
(select 'ATCINEMA', film.*,film_type.NAME as FILM_TYPE_NAME,   film_images.IMAGES_PATH as IMAGES_PATH
 from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id
  where film_images.id = (select max(id) from film_images where film_id=film.id )
  and film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =2) limit 5) 
    union
(select 'PREVIEW', film.*,film_type.NAME as FILM_TYPE_NAME,   film_images.IMAGES_PATH as IMAGES_PATH
 from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id
  where film_images.id = (select max(id) from film_images where film_id=film.id )
  and film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =3) limit 5) 


select * from menu

select film_type.name , count( distinct(film.id)) as nbFilm
from film_type join film on film.film_type_id=film_type.id
group by film_type.name
order by film_type.name 

(select 'genre', genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
from genre join film_genre on genre.id=film_genre.genre_id
group by genre.name order by genre.name )
union
(select 'origine', 'origine',count(id) as nbFilm
from film
where year_production <1930)
union
(select 'classique ', 'classique',count(id) as nbFilm
from film
where year_production >=1930 and year_production <=1945)
union
(select 'classique ', 'Les années 50',count(id) as nbFilm
from film
where year_production >=1946 and year_production <=1958)
union
(select 'classique ', 'Les années 60',count(id) as nbFilm
from film
where year_production >=1959 and year_production <=1967)
union
(select 'classique ', 'Les années 70',count(id) as nbFilm
from film
where year_production >=1968 and year_production <=1983)
union
(select 'classique ', 'Les années 80',count(id) as nbFilm
from film
where year_production >=1984 and year_production <=1991)
union
(select 'classique ', 'Les années 90',count(id) as nbFilm
from film
where year_production >=1992 and year_production <=2001)
union
(select 'classique ', 'Les années 2000',count(id) as nbFilm
from film
where year_production >=2002 and year_production <=2010)
union
(select 'classique ', 'Les années 2010',count(id) as nbFilm
from film
where year_production >=2010 and year_production <=2017)
union
(select 'classique ', 'Après 2017',count(id) as nbFilm
from film
where year_production >2017)




(select country.name as name , count(distinct(film_country.film_id))  as nbFilm
from country join film_country on country.id=film_country.country_id
group by country.name order by country.name )
union
(select distributor.name as name , count(distinct(film_distributor.film_id))  as nbFilm
from distributor join film_distributor on distributor.id=film_distributor.distributor_id
group by distributor.name order by distributor.name )

select 'classique', count(id) as nbFilm
from film
where year_production <1930

select * from film

select film.*,film_type.NAME as FILM_TYPE_NAME , COALESCE(film_images_large.IMAGES_PATH,0) as  IMAGES_LARGE_PATH
from film  left join film_type on film.film_type_id=film_type.id  left join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select max(id) from film_images_large where film_id=film.id )
where film.id =2

delete from film_images;
delete from film_images_large;


select actor.name
from actor join film_actor on actor.id=film_actor.actor_id
join film on film.id =film_actor.film_id
where film.id=2

select director.name
from director join film_director on director.id=film_director.director_id
join film on film.id =film_director.film_id
where film.id=?

select distributor.name
from distributor join film_distributor on distributor.id=film_distributor.distributor_id
join film on film.id =film_distributor.film_id
where film.id=?

select genre.name
from genre join film_genre on genre.id=film_genre.genre_id
join film on film.id =film_genre.film_id
where film.id=?

select productor.name
from productor join film_productor on productor.id=film_productor.productor_id
join film on film.id =film_productor.film_id
where film.id=1

select review.*
from review join film on review.film_id=film.id
join spectator on spectator.id =review.spectator_id
where film.id=1

select film_images.images_path
from film_images join film on film_images.film_id=film.id
where film.id=1

select film_images_large.images_path	
from film_images_large join film on film_images_large.film_id=film.id
where film.id=1


select * from film 
left join film_images on film.id=film_images.film_id and film_images.id =(select max(id) from film_images where film_id=film.id)
where film.id=2

select images_path , count(film_id)
from film_images_large
group by images_path
having count(film_id)>1
order by count(film_id)


select distinct(film_id)
from film_images_large
select count(1) from film_images_large

SELECT idCine from film where film_type_id =12 order by idCine desc
117076

select distinct(genre_id) from film_genre where film_id=43286


 select film.*,film_type.NAME as FILM_TYPE_NAME , COALESCE(film_images.IMAGES_PATH,0) as IMAGES_PATH , COALESCE(film_images_large.IMAGES_PATH,0) as  IMAGES_LARGE_PATH   
 from film   join film_type on film.film_type_id=film_type.id  
 left join  film_images on film_images.film_id=film.id and film_images.id  = (select max(id) from film_images where film_id=film.id ) 
 left join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select max(id) from film_images_large where film_id=film.id ) 
 where film.id =(select max(id) from film where id <4279) or film.id=(select max(id) from film where id > 4279)
 order by film.id limit 2
 
 select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id in (select distinct(genre_id) from film_genre where film_id =43286) limit 2

 select film.*,film_type.NAME as FILM_TYPE_NAME , COALESCE(film_images.IMAGES_PATH,0) as IMAGES_PATH , COALESCE(film_images_large.IMAGES_PATH,0) as  IMAGES_LARGE_PATH   from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select max(id) from film_images where film_id=film.id ) left join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select max(id) from film_images_large where film_id=film.id )  
 where film.id in    (select film.id from film join film_menu on film.id=film_menu.film_id and film_menu.menu_id =1    WHERE MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )  


 (select 'genre', genre.id  as id, genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
from genre join film_genre on genre.id=film_genre.genre_id
group by genre.name order by genre.name )
union
(select 'annee', '-1930', 'origine (avant 1930)',count(id) as nbFilm
from film
where year_production <1930)
union
(select 'annee', '1930-1945', 'classique (1930-1945)',count(id) as nbFilm
from film
where year_production >=1930 and year_production <=1945)
union
(select 'annee', '1956-1958', 'Les années 50',count(id) as nbFilm
from film
where year_production >=1946 and year_production <=1958)
union
(select 'annee', '1959-1967', 'Les années 60',count(id) as nbFilm
from film
where year_production >=1959 and year_production <=1967)
union
(select 'annee', '1968-1983','Les années 70',count(id) as nbFilm
from film
where year_production >=1968 and year_production <=1983)
union
(select 'annee', '1984-1991','Les années 80',count(id) as nbFilm
from film
where year_production >=1984 and year_production <=1991)
union
(select 'annee', '1992-2001','Les années 90',count(id) as nbFilm
from film
where year_production >=1992 and year_production <=2001)
union
(select 'annee', '2002-2010','Les années 2000',count(id) as nbFilm
from film
where year_production >=2002 and year_production <=2010)
union
(select 'annee', '2010-2017','Les années 2010',count(id) as nbFilm
from film
where year_production >=2010 and year_production <=2017)
union
(select 'annee', '2017-',count(id) as nbFilm
from film
where year_production >2017)

 select film.*,film_type.NAME as FILM_TYPE_NAME , COALESCE(film_images.IMAGES_PATH,0) as IMAGES_PATH , COALESCE(film_images_large.IMAGES_PATH,0) as  IMAGES_LARGE_PATH   from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select max(id) from film_images where film_id=film.id ) left join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select max(id) from film_images_large where film_id=film.id )  where film.id in    (select film.id from film join film_genre on film.id=film_genre.film_id and film_genre.genre_id =6    where MATCH(film.title,film.synopsis,film.original_title) AGAINST ('in space') > 0 ) 
 
 
  (select 'genre', genre.id  as id, genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
            from genre join film_genre on genre.id=film_genre.genre_id join film on film.id=film_genre.film_id
            where   film.film_type_id!=12 
            group by genre.name order by genre.name )
            union
            (select 'annee', '-1930', 'origine (avant 1930)',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  and year_production <1930)
            union
            (select 'annee', '1930-1945', 'classique (1930-1945)',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1930 and year_production <=1945)
            union
            (select 'annee', '1956-1958', 'Les années 50',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1946 and year_production <=1958)
            union
            (select 'annee', '1959-1967', 'Les années 60',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1959 and year_production <=1967)
            union
            (select 'annee', '1968-1983','Les années 70',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1968 and year_production <=1983)
            union
            (select 'annee', '1984-1991','Les années 80',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1984 and year_production <=1991)
            union
            (select 'annee', '1992-2001','Les années 90',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=1992 and year_production <=2001)
            union
            (select 'annee', '2002-2010','Les années 2000',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=2002 and year_production <=2010)
            union
            (select 'annee', '-20072017','Les années 2010',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >=2010 and year_production <=2017)
            union
            (select 'annee', '2017-', 'Après 2017',count(id) as nbFilm
            from film
            where   film.film_type_id!=12  year_production >2017)

select id, idcine from film 
where id not in ( select distinct(film_id) from film_images ) order by idcine

select count(distinct(film_id)) from film_images_large 



select * from film_images_large where film_id=53887 and images_path='18419994.jpg'
update film_images set IS_POSTER=null

alter table temp_film add IMAGE_PR varchar(100);
alter table temp_serie add IMAGE_PR varchar(100);

select count(1) from film_images where IS_POSTER is true

select id from film WHERE  MATCH(title,synopsis,original_title) AGAINST (?) > 0

select wishlist_line.* from wishlist join wishlist_line on wishlist.id=wishlist_line.wishlist_id   where wishlist.spectator_id =1

alter table spectator add AVATAR varchar(50)

select review.* from review 
where wishlist.spectator_id =1

select * from spectator
delete from wishlist
where id >1


select * from wishlist_line  














