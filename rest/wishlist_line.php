<?php
include_once __DIR__ . '/../services/spectatorservice.php';

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods:   OPTIONS, POST, DELETE");
require_once '../vendor/autoload.php';

use Firebase\JWT\JWT;

if (! isset($_SERVER['REQUEST_METHOD']) || ! isset($_SERVER['REQUEST_URI'])) {
    echo json_encode(array(
        'code' => 400,
        'status' => 'error',
        'message' => 'Bad Request'
    ));
}
// check the method
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        createLineToWishlist();
        break;
    case 'GET':
        break;
        
    case 'DELETE':
        removeLineFromWishlist();
        break;
    default:
        echo json_encode(array(
        'code' => 403,
        'status' => 'error',
        'message' => 'Method Not Allowed'
            ));
        exit();
}

function removeLineFromWishlist() { // get json
    $data = json_decode(file_get_contents("php://input"), true);
    //check jwt for spectator
    $jwt = isset($data['jwt']) ? $data['jwt'] : null;
    $idWistlist_Line = isset($data['idWistlist_Line']) ? $data['idWistlist_Line'] : null;
    if (isset($jwt) && isset($idWistlist_Line)) {
        // validate jwt
        $key = JWT_SECRET;
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            
            $spectatorervice = new SpectatorService();
            if( $decoded_array){
                $spectatorervice->removeWishlistLine($decoded_array['userid'],$idWistlist_Line);
                //ar_dump($wistlistLine);
                $result = array(
                    'code' => 200,
                    'status' => 'success',
                    'data' => ''
                );
            }
            
            
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    } else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }
    
    echo json_encode($result);
}
function createLineToWishlist() {
    // get json
    $data = json_decode(file_get_contents("php://input"), true);
    //check jwt for spectator
    $jwt = isset($data['jwt']) ? $data['jwt'] : null;
    $idFilm = isset($data['idFilm']) ? $data['idFilm'] : null;
    $idWistlist = isset($data['idWistlist']) ? $data['idWistlist'] : null;
    //var_dump($idFilm);
    if (isset($jwt) && isset($idFilm) && isset($idWistlist)) {
        // validate jwt
        $key = JWT_SECRET;
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            
            $spectatorervice = new SpectatorService();
            if( $decoded_array){
                $wistlistLine=$spectatorervice->createWishlistLine($idFilm,$decoded_array['userid'],$idWistlist);
                //ar_dump($wistlistLine);
                $result = array(
                    'code' => 200,
                    'status' => 'success',
                    'data' => $wistlistLine
                );
            }
            
            
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    } else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }
    
    echo json_encode($result);
}
?>