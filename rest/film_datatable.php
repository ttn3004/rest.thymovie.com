<?php
include_once __DIR__ . '/../services/pagingservice.php';
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
$params = file_get_contents('php://input');

$pagingservice = new Pagingservice($params);
echo json_encode($pagingservice->get());


?>