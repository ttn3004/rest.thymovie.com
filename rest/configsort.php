<?php

include_once __DIR__ . '/../services/filmservice.php';
$method = $_SERVER['REQUEST_METHOD'];

$request = "";
if(isset($_SERVER['PATH_INFO'])){
    $request=explode('/', trim($_SERVER['PATH_INFO'],'/'));
}
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");

$filmservice = new FilmService();
if($method == "GET"){
    try {
        http_response_code(200);
        $arr=$filmservice->getSortConfig($request);
        echo json_encode($arr);
    } catch (Exception $e) {
        $error= "HTTP/1.1 400" . $e->getMessage();
        header($error);
        echo json_encode(json_decode ("{}"));
    }
}

?>