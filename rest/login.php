<?php
include_once __DIR__ . '/../services/spectatorservice.php';

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
require_once '../vendor/autoload.php';

use Firebase\JWT\JWT;

// get the request method
if (! isset($_SERVER['REQUEST_METHOD']) || ! isset($_SERVER['REQUEST_URI'])) {
    echo json_encode(array(
        'code' => 400,
        'status' => 'error',
        'message' => 'Bad Request'
    ));
}

// check the method
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        validateUserCredential();
        break;

    case 'GET':
        getUser();
        break;
    case 'OPTIONS':
        break;
    default:
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Method Not Allowed'
        ));
        exit();
}

function validateUserCredential()
{
    // get json
    $data = json_decode(file_get_contents("php://input"), true);
    if (empty($data)) {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Data missing'
        );
    } else {

        if (isset($data['email'])) {
            $email = $data['email'];
        } else {
            $email = null;
        }

        if (isset($data['password'])) {
            $password = $data['password'];
        } else {
            $password = null;
        }

        // check login credentials
        $result = array(
            'code' => '0',
            'status' => 'error',
            'message' => 'No match found'
        );
        $spectatorervice = new SpectatorService();
        $user = $spectatorervice->getUserAccountData($email);
        if ($user != null && password_verify($password, $user['password'])) {
            $result = array(
                'code' => 200,
                'status' => 'success',
                'message' => 'Valid login credentials.',
                'userid' => $user['id']
            );
        }

        // on success generate jwt
        if (isset($result['status']) && $result['status'] === 'success') {
            $userid = $result['userid'];
            $issuedAt = time();
            $expirationTime = $issuedAt + TIME_EXPIRE; // jwt valid for 60 seconds from the issued time
            $payload = array(
                'userid' => $userid,
                'iat' => $issuedAt,
                'exp' => $expirationTime
            );
            $key = JWT_SECRET;
            $alg = 'HS256';
            $jwt = JWT::encode($payload, $key, $alg);
            $result['jwt'] = $jwt;
        }
    }

    echo json_encode($result);
}

function getUser()
{
    // get the jwt from url
    $jwt = isset($_GET['jwt']) ? $_GET['jwt'] : null;

    if (isset($jwt)) {
        // validate jwt
        $key = JWT_SECRET;
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            $spectatorervice = new SpectatorService();
            $user=$spectatorervice->getUserAccountData($decoded_array['userid']);
            unset($user['password']);
            $result = array(
                'code' => 200,
                'status' => 'success',
                'data' => $user,
                'jwt_payload' => $decoded_array
            );
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    } else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }

    echo json_encode($result);
}
?>