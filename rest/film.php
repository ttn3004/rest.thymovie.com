<?php

include_once __DIR__ . '/../services/filmservice.php';

$method = $_SERVER['REQUEST_METHOD'];   

$request = "";
if(isset($_SERVER['PATH_INFO'])){
    $request=explode('/', trim($_SERVER['PATH_INFO'],'/'));
}
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");

$filmservice = new FilmService();
if($method == "GET"){
    try {
        http_response_code(200);
        $film_arr=$filmservice->get($request);
        echo json_encode($film_arr);
    } catch (Exception $e) {
        $error= "HTTP/1.1 400" . $e->getMessage();
        header($error);
        echo json_encode(json_decode ("{}"));
    }
}elseif ($method == "POST" && $request == ""){
    $data = json_decode(file_get_contents("php://input"));
    $film= $filmservice->create($data);
    header("HTTP/1.1 201 CREATED");
    echo json_encode($film);
}elseif ($method == "PUT" && $request == ""){
    $data = json_decode(file_get_contents("php://input"));
    $film= $filmservice->update($data);
    header("HTTP/1.1 200 UPDATE");
    echo json_encode($film);
}elseif ($method == "DELETE" && $request != ""){
    $film= $filmservice->delete($request);
    header("HTTP/1.1 200 DELETE");
}



?>