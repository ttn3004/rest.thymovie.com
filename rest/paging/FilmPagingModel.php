<?php
require_once ('FilterModel.php');
require_once ('SortModel.php');
require_once ('MenuFilterModel.php');
class FilmPagingModel {
    public $page;
    public $totalPage ;
    public $pageSize ;
    public $sorted =array();
    public $filtered = array();
    public $totalSize;
    public $listFiltered = array();
    public $menuFiltered = array();
    public $filmType;
    
    public $searchByKeyword;
    
    
    
    function __construct($params)
    {
        $jsonArray = json_decode($params, true);
        foreach($jsonArray as $key => $val)
        {
            if(property_exists(__CLASS__,$key))
            {
                if($key == "sorted"){
                    $arr = $val;
                    foreach ($arr as & $value) {
                        $sort=new SortModel($value);
                        array_push($this->sorted,$sort) ;
                    }
                }elseif($key == "filtered"){
                    $arr = $val;
                    foreach ($arr as & $value) {
                        $filt=new FilterModel($value);
                        array_push($this->filtered,$filt) ;
                    }
                }elseif($key == "menuFiltered"){
                    $arr = $val;
                    foreach ($arr as & $value) {
                        $filt=new MenuFilterModel($value);
                        array_push($this->menuFiltered,$filt) ;
                    }
                }
                else
                    $this->$key =  $val;
            }
        }
    }
}