<?php
include_once __DIR__ . '/../services/spectatorservice.php';

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
require_once '../vendor/autoload.php';

use Firebase\JWT\JWT;
// get the request method
if (! isset($_SERVER['REQUEST_METHOD']) || ! isset($_SERVER['REQUEST_URI'])) {
    echo json_encode(array(
        'code' => 400,
        'status' => 'error',
        'message' => 'Bad Request'
    ));
}

// check the method
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        createReview();
        break;
        
    case 'GET':
        getUser();
        break;
        
    default:
        echo json_encode(array(
        'code' => 403,
        'status' => 'error',
        'message' => 'Method Not Allowed'
            ));
        exit();
}

function createReview()
{
    // get json
    $data = json_decode(file_get_contents("php://input"), true);
    //check jwt for spectator
    $jwt = isset($data['jwt']) ? $data['jwt'] : null;
    $idFilm = isset($data['idFilm']) ? $data['idFilm'] : null;
    $comment = isset($data['comment']) ? $data['comment'] : null;
    //var_dump($idFilm);
    if (isset($jwt) && isset($idFilm) && isset($comment)) {
        // validate jwt
        $key = JWT_SECRET;
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            
            $spectatorervice = new SpectatorService();
            if( $decoded_array){
                $review=$spectatorervice->createReview($idFilm,$decoded_array['userid'],$comment);
                $result = array(
                    'code' => 200,
                    'status' => 'success',
                    'data' => $review
                );
            }
            
            
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    } else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }
    
    echo json_encode($result);
}


?>