<?php
include_once __DIR__ . '/../services/datatableservice.php';
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
$params = $_REQUEST;

$datatableService = new DataTableService($params);

echo json_encode($datatableService->get());


?>