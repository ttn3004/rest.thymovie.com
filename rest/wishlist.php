<?php
include_once __DIR__ . '/../services/spectatorservice.php';

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods:   OPTIONS, POST, DELETE");
require_once '../vendor/autoload.php';

use Firebase\JWT\JWT;

if (! isset($_SERVER['REQUEST_METHOD']) || ! isset($_SERVER['REQUEST_URI'])) {
    echo json_encode(array(
        'code' => 400,
        'status' => 'error',
        'message' => 'Bad Request'
    ));
}

// check the method
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        createWishlist();
        break;
        
    case 'GET':
        break;
    case 'DELETE':
        deleteWishlist();
        break;
    default:
        echo json_encode(array(
        'code' => 403,
        'status' => 'error',
        'message' => 'Method Not Allowed'
            ));
        exit();
}

function createWishlist(){
    $data = json_decode(file_get_contents("php://input"), true);
    //check jwt for spectator
    $jwt = isset($data['jwt']) ? $data['jwt'] : null;
    $newWl_label= isset($data['newWl_label']) ? $data['newWl_label'] : null;
    if (isset($jwt) && isset($newWl_label)) {
        // validate jwt
        $key = JWT_SECRET;
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            
            $spectatorervice = new SpectatorService();
            if( $decoded_array){
                $wishlistCreated=$spectatorervice->createWishlist($decoded_array['userid'],$newWl_label);
                $result = array(
                    'code' => 200,
                    'status' => 'success',
                    'data' => $wishlistCreated
                );
            }
            
            
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    }else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }
    echo json_encode($result);
}
function deleteWishlist(){
    $data = json_decode(file_get_contents("php://input"), true);
    //check jwt for spectator
    $jwt = isset($data['jwt']) ? $data['jwt'] : null;
    $idWishlist= isset($data['idWishlist']) ? $data['idWishlist'] : null;
    if (isset($jwt) && isset($idWishlist)) {
        // validate jwt
        $key = JWT_SECRET;  
        try {
            $decoded = JWT::decode($jwt, $key, array(
                'HS256'
            ));
            $decoded_array = (array) $decoded;
            
            $spectatorervice = new SpectatorService();
            if( $decoded_array){
                $spectatorervice->removeWishlist($decoded_array['userid'],$idWishlist);
                $result = array(
                    'code' => 200,
                    'status' => 'success',
                    'data' => ''
                );
            }
            
            
        } catch (\Exception $e) {
            $result = array(
                'code' => 0,
                'status' => 'error',
                'message' => 'Token invalide - Authentication échouée !'
            );
        }
    }else {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => 'Les paramètres de Token JWT ne sont pas présents!'
        );
    }
    echo json_encode($result);
}

?>