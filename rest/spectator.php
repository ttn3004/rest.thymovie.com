<?php

include_once __DIR__ . '/../services/spectatorservice.php';


header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
require_once '../vendor/autoload.php';

use Firebase\JWT\JWT;

// get the request method
if (!isset($_SERVER['REQUEST_METHOD']) || !isset($_SERVER['REQUEST_URI'])) {
    echo json_encode(array(
        'code' => 400,
        'status' => 'error',
        'message' => 'Bad Request'
    ));
}
$method = $_SERVER['REQUEST_METHOD'];

$request = "";
if(isset($_SERVER['PATH_INFO'])){
    $request=explode('/', trim($_SERVER['PATH_INFO'],'/'));
}

$spectatorervice = new SpectatorService();
if($method == "GET"){
    getSpectator();
}else if ($method == "POST" && $request == ""){
    $data = json_decode(file_get_contents("php://input"));
    $result = array(
        'code' => 0,
        'status' => 'error',
        'message' => 'Data missing'
    );
    
    try {
        $user= $spectatorervice->create($data);
        
        if ($user != null) {
            $result = array(
                'code' => 200,
                'status' => 'success',
                'message' => 'Valid login credentials.',
                'userid' => $user['id']
            );
            $userid = $result['userid'];
            $issuedAt = time();
            $expirationTime = $issuedAt + TIME_EXPIRE; // jwt valid for 60 seconds from the issued time
            $payload = array(
                'userid' => $userid,
                'iat' => $issuedAt,
                'exp' => $expirationTime
            );
            $key = JWT_SECRET;
            $alg = 'HS256';
            $jwt = JWT::encode($payload, $key, $alg);
            $result['jwt'] = $jwt;
        }
    } catch (Exception $e) {
        $result = array(
            'code' => 0,
            'status' => 'error',
            'message' => $e->getMessage()
        );
    }
    
    
   
    echo json_encode($result);
}
?>