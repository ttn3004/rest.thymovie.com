<?php
/**
 * Author: Yusuf Shakeel
 * Date: 15-Apr-2018 Sun
 * Version: 1.0
 *
 * File: index.php
 * Description: This file contains the index page.
 *
 * GitHub: https://github.com/yusufshakeel/jwt-php-project
 *
 * MIT License
 * Copyright (c) 2018 Yusuf Shakeel
 */
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.0/css/select.bootstrap4.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://editor.datatables.net/extensions/Editor/css/editor.bootstrap4.min.css"/>
 
    <title>Login by Jwt</title>
</head>
<body>
<div class="row">
    <div class= "col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">film List'</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                     <table id="example" class="table table-striped table-bordered" style="width:100%">
                     	<thead>
                            <tr>
                            	<th>id</th>
                                <th>titre </th>
                                <th>synopsis</th>
                                <th>duration</th>
                            </tr>
                        </thead>
                     </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>                
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 offset-sm-4 col-sm-4">
            <h3 class="text-center">Login jwt</h3>
            <br>
            <h4 class="text-center">Login</h4>
            <form id="login">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email"
                           maxlength="255"
                           class="form-control"
                           id="login-email"
                           required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password"
                           maxlength="32"
                           class="form-control"
                           id="login-password"
                           required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Log in">
                </div>
            </form>
        </div>
        <div class="col-xs-12 offset-sm-4 col-sm-4">
            <hr>
            <div id="result-container"></div>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#example').dataTable({
		"bProcessing": true,
	 	"serverSide": true,
	 	"ajax":{
	        url :"http://localhost:8167/thymovie/rest/film_data.php",
	        type: "POST",
	        error: function(){
	          $("#post_list_processing").css("display","none");
	        }
	  	}
	});
	 
});
</script>

</body>
</html>