<?php
include_once __DIR__ . '/../config/dbclass.php';
require_once __DIR__ . '/../config/meta.php';
require_once 'sql.php';
require_once 'iservice.php';
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

// remove all warning
class FilmService extends IService
{
    
    public function get($request)
    {
        if (! is_array($request))
            throw new Exception("Mauvais paramètres.");
        else if (is_numeric($request[0])) {
            return $this->getById($request[0]);
        } else {

            $query = "";
            $parameter = null;
            $cat = null;
            if ($request[0] == 'keyword') {
                if (strlen($request[1]) <= 2)
                    throw new Exception("Mauvais paramètres pour " . $request[0]);
            } else if ($request[0] == 'menuHome')
                $parameter = null;
            else if (! is_string($request[0]) || ! is_numeric($request[1]))
                throw new Exception("Mauvais paramètres pour " . $request[0]);

            $cat = $request[0];
            $parameter = $request[1];
            switch ($cat) {
                case "menuHome":
                    $query = FILM_REQUEST["menuHome"];
                    break;
                case "menu":
                    $query = FILM_REQUEST["menu"];
                    break;
                case "genre":
                    $query = FILM_REQUEST["genre"];
                    break;
                case "country":
                    $query = FILM_REQUEST["country"];
                    break;
                case "film_type":
                    $query = FILM_REQUEST["film_type"];
                    break;
                case "director":
                    $query = FILM_REQUEST["director"];
                    break;
                case "productor":
                    $query = FILM_REQUEST["productor"];
                    break;
                case "distributor":
                    $query = FILM_REQUEST["distributor"];
                    break;
                case "actor":
                    $query = FILM_REQUEST["actor"];
                    break;
                case "keyword":
                    $query = FILM_REQUEST["keyword"];
                    break;
                default:
                    $query = FILM_REQUEST["menu"];
                    break;
            }
            return parent::executeGet($query, $parameter);
        }
    }

    public function getById($id)
    {
        if (! is_numeric($id))
            throw new Exception("Id devrait être un nombre entier.");
       return $this->getFilmDetail($id);
    }
    public function getFilmDetail($id)
    {
            $film =parent::executeGetOneNotClose(FILM_REQUEST["getOne"], $id);
            if($film){
                //actor
                $film["actor"] = parent::executeGetNotClose(FILM_REQUEST["actor"], $id);
                //director
                $film["director"] = parent::executeGetNotClose(FILM_REQUEST["director"], $id);
                //productor
                $film["distributor"] = parent::executeGetNotClose(FILM_REQUEST["distributor"], $id);
                //productor
                $film["productor"] = parent::executeGetNotClose(FILM_REQUEST["productor"], $id);
                //images
                $film["genre"] = parent::executeGetNotClose(FILM_REQUEST["genre"], $id);
                //productor
                $film["country"] = parent::executeGetNotClose(FILM_REQUEST["country"], $id);
                //genres
                $film["film_images"] = parent::executeGetNotClose(FILM_REQUEST["film_images"], $id);
                //images large  
                $film["film_images_large"] = parent::executeGetNotClose(FILM_REQUEST["film_images_large"], $id);
                //video
                $film["film_videos"] = parent::executeGetNotClose(FILM_REQUEST["film_videos"], $id);
                //reviews
                $film["film_reviews"] = parent::executeGetNotClose(FILM_REQUEST["film_reviews"], $id);
                $querySuggest= str_replace("@type", $film['film_type_id'],str_replace("@limit",2,str_replace("@idFilm",$id,FILM_REQUEST["suggest"])));
                //var_dump($querySuggest);
                $film["film_suggest"] = parent::executeGetNotClose($querySuggest, $id);
                $this->connection->close();
            }
    
            //var_dump($film);
            return $film; // Parse to JSON and print.
         
    }
    public function create($data)
    {
        try {
            $query = FILM_REQUEST_CREATE;
            $stmt = $this->connection->prepare($query);
            $stmt->bind_param("ss", $data->title, $data->synopsis);
            $stmt->execute();
            $id = mysqli_insert_id($this->connection);
            return $this->getById($id); //
        } catch (Exception $e) {
            throw new Exception("Une erreur est survenue.", $e);
        }
    }

    public function update($data)
    {
        try {
            $query = FILM_REQUEST_UPDATE;
            $stmt = $this->connection->prepare($query);
            $stmt->bind_param("ssi", $data->title, $data->synopsis, $data->id);
            $stmt->execute();
            return $this->getById($data->id);
        } catch (Exception $e) {
            throw new Exception("Une erreur est survenue.", $e);
        }
    }

    public function delete($id)
    {
        if (! is_numeric($id))
            throw new Exception("Une erreur est survenue.");
        try {
            $query = FILM_REQUEST_DELETE;
            $stmt = $this->connection->prepare($query);
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->close();
            $this->connection->close();
        } catch (Exception $e) {
            throw new Exception("Une erreur est survenue.", $e);
        }
    }
    function getSortConfig($request){
        
        $query = FILM_REQUEST[$request[0]."_sortMenu"];
        return parent::executeGet($query, null);
    }
    
    public function getIdFilm($idFilm){
        return parent::executeGetOne(FILM_REQUEST["film_Get_Id"], $idFilm);
    }
    
    
}
?>