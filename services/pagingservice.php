<?php
include_once __DIR__ . '/../config/dbclass.php';
require_once __DIR__ . '/../config/meta.php';
require_once 'sql.php';
require_once ('paging/FilmPagingModel.php');
require_once 'iservice.php';
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

class Pagingservice extends IService
{

    public $columns = array(
        0 => 'id',
        1 => 'title',
        2 => 'synopsis',
        3 => 'duration',
        4 => 'updated_at'
    );

    public $sql_query = "";

    public $sqlTot = "";

    public $sqlRec = "";

    public $filmPagingModel;

    function __construct($params)
    {
        
        parent::__construct();
        $this->filmPagingModel = new FilmPagingModel($params);
        $posLastWhere = 0;
        $posLastParenthesis = 99999;
        $orderStr = " order by  ";

        if (! empty($this->filmPagingModel->sorted)) {
            foreach ($this->filmPagingModel->sorted as $item) {
                $orderStr .= $item->id . ($item->desc ? " DESC , " : " ASC , ");
            }
        }
        $orderStr .= "  updated_at DESC  ";

        $type = $this->filmPagingModel->filmType;
        if( empty ($this->filmPagingModel->searchByKeyword)){
            $this->sql_query = FILM_REQUEST[$type . "_Datatable"];
            
            if (! empty($this->filmPagingModel->menuFiltered)) {
                $filteredModel = $this->filmPagingModel->menuFiltered[0];
                $this->filterBy( $filteredModel,FILM_REQUEST[$type."_Datatable_By_" . $filteredModel->id ]);
            }
            //desactiver pour filtrer les films par menu (genre, annee) ==> a verifier pour quoi ce code
//             if($type == 'all')
//                 $this->sql_query.=" where 1<2 ";
        
        }else{
            $this->sql_query = FILM_REQUEST[$type . "_keyword"];
            $searchKeyExplode = explode('-', $this->filmPagingModel->searchByKeyword);
            $this->sql_query = str_replace('@idParam', implode(' ', $searchKeyExplode), $this->sql_query );
            $posLastParenthesis = strripos($this->sql_query, ')', 1)-1;
        }
        
        $posLastWhere = strripos($this->sql_query, 'where', 1);
        $longToSub=$posLastParenthesis-$posLastWhere;
        
        
        $filteredWhere = "";//dans le cas ou on doit filtrer plusieurs colonnes 
        if (! empty($this->filmPagingModel->filtered)) {
            foreach ($this->filmPagingModel->filtered as $item) {
                $filteredWhere .= " AND " . $item->id . " LIKE '%" . $item->value . "%' ";
            }
            $this->sql_query = $this->sql_query . $filteredWhere;
            $this->sqlTot .=" select count(1) from film join film_type on film.film_type_id=film_type.id  " . substr($this->sql_query , $posLastWhere, $longToSub). $filteredWhere;//strlen($this->sql_query)
            $this->sqlRec .= $this->sql_query;
        }else{
            $this->sqlTot .=" select count(1) from film join film_type on film.film_type_id=film_type.id  " . substr($this->sql_query , $posLastWhere, $longToSub);//strlen($this->sql_query)
            $this->sqlRec .= $this->sql_query;
        }        
        
        $endIndex = $this->filmPagingModel->page == 0 ? $this->filmPagingModel->pageSize : ($this->filmPagingModel->page+1) * $this->filmPagingModel->pageSize;
        $startIndex = $endIndex - $this->filmPagingModel->page == 0 ? 0 : $endIndex - $this->filmPagingModel->pageSize;

        $this->sqlRec .= $orderStr . "  LIMIT " . $this->filmPagingModel->pageSize . " offset " . $startIndex . " ";
    
    }

    public function get()
    {

        $totalRows = mysqli_query($this->connection, $this->sqlTot) or die("Database Error" . mysqli_error($this->connection));
        $row = $totalRows->fetch_row();
        $totalRecords = $row[0];

        $data = parent::executeRq($this->sqlRec);
//         $queryRecords = mysqli_query($this->connection, $this->sqlRec) or die("Error to Get the Post details.");
//         while ($row = mysqli_fetch_row($queryRecords)) {
//             array_push($data, $row);
//         }
        if ($data == null)
            $data = array();
        $pages = ceil($totalRecords / $this->filmPagingModel->pageSize);
        $this->filmPagingModel->totalSize = $totalRecords;
        $this->filmPagingModel->totalPage = $pages;
        $this->filmPagingModel->listFiltered = $data;
        return $this->filmPagingModel;
    }

    public function filterBy($filteredModel,$query)
    {
        if ($filteredModel->id == 'annee') {    
            $strDate = $filteredModel->value;
            if (strpos($strDate, '-') === 0) {
                $this->sql_query = str_replace('@whereYear', ' < ' . substr($strDate, 1, strlen($strDate)), $query );
            } else if (strpos($strDate, '-') === strlen($strDate) - 1) {
                $this->sql_query = str_replace('@whereYear', ' > ' . substr($strDate, 0, strlen($strDate) - 1), $query);
            } else {
                $dateArr = explode("-", $strDate);
                $this->sql_query = str_replace('@whereYear', ' between ' . $dateArr[0] . ' AND ' . $dateArr[1], $query);
            }
        } else {
            $searchKeyExplode = explode('-', $filteredModel->value);
            $this->sql_query = str_replace('@idParam', implode(' ', $searchKeyExplode), $query);
        }
    }
    
    
    public function executeRq($query){
        if ($stmt = $this->connection->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $count = $stmt->num_rows();
            if ($count > 0) {
                $row = $this->bind_result_array($stmt);
                $film_arr = array();
                while ($stmt->fetch()) {
                    array_push($film_arr, $this->getCopy($row));
                }
                $stmt->close();
                $this->connection->close();
                return $film_arr; // Parse to JSON and print.
            } else {
                $stmt->close();
                $this->connection->close();
                return array();
            }
        } else {
            $this->connection->close();
            return array();
        }
    }
    
}