<?php
include_once __DIR__ . '/../config/dbclass.php';
require_once __DIR__ . '/../config/meta.php';

require_once 'sql.php';
require_once 'iservice.php';
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
// JWT secret
define('JWT_SECRET', 'this-is-the-secret');
define('TIME_EXPIRE', 3600);

// 1h
// remove all warning
class SpectatorService extends IService
{

    public function getById($id)
    {
        if (! is_numeric($id))
            throw new Exception("Id devrait être un nombre entier.");
            $query = SPECT_REQUEST_SELECT_BYID ;
        return parent::executeGetOne($query, $id);
    }

    public function create($data)
    {

        // check email
        $query = SPECT_REQUEST_LOGIN;
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("s", $data->email);
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows();
        if ($count > 0) {
            throw new Exception("L'Email est déjà utilisé");
        }
        $query = SPECT_REQUEST_CREATE;
        $stmt = $this->connection->prepare($query);
        $password_hash = password_hash((string) $data->password, PASSWORD_BCRYPT);
        $pos = strpos($data->email, '@', 1);
        $name = substr($data->email, 0, $pos);
        $stmt->bind_param("sss", $name, $data->email, $password_hash);
        $stmt->execute();
        $id = mysqli_insert_id($this->connection);
        return $this->getById($id); //
    }

    public function getUserAccountData($getBy)
    {
        try {
            // var_dump($getBy);
            $query = null;
            if (is_numeric($getBy)) {
                $query = SPECT_REQUEST["getById"];
            } else {
                $query = SPECT_REQUEST["getByEmail"];
            }
            $stmt = $this->connection->prepare($query);
            if (is_numeric($getBy))
                $stmt->bind_param('i', $getBy);
            else
                $stmt->bind_param('s', $getBy);

            $stmt->execute();
            $stmt->store_result();
            $count = $stmt->num_rows();

            if ($count > 0) {
                $row = $this->bind_result_array($stmt);
                /* fetch values */
                $userFounded = null;
                if ($stmt->fetch()) {
                    $userFounded = $this->getCopy($row);
                }
                if ($userFounded && $getBy) {
                    $arrWs = array();
                    $arr = parent::executeGetNotClose(SPECT_REQUEST["getWishList_BySpect"], $getBy);
                    if ($arr) {
                        foreach ($arr as $ws) {
                            $ws['wistlist_lines'] = parent::executeGetNotClose(SPECT_REQUEST["getWishListLine_ByWishList"], $ws['id']);
                            array_push($arrWs, $ws);
                        }
                    }
                    $userFounded["wishlists"] = $arrWs;
                }

                $stmt->close();
                $this->connection->close();
                return $userFounded;
            } else {
                $stmt->close();
                $this->connection->close();
                return null;
            }
        } catch (Exception $e) {
            throw new Exception("Une erreur est survenue.", $e);
        }
    }

    public function createReview($idFilm, $idSpectator, $comment)
    {
        try {
            $spect = parent::executeGetOneNotClose(SPECT_REQUEST["getById"], $idSpectator);
            $film = parent::executeGetOneNotClose(FILM_REQUEST["film_Get_Id"], $idFilm);
            if (! $film)
                throw new Exception("Le film n'existe pas");
            if (! $spect)
                throw new Exception("Le spectateur n'existe pas");

                $query = SPECT_REQUEST_CREATE_REVIEW;
            $stmt = $this->connection->prepare($query);
            $stmt->bind_param('iis', intval($idFilm), intval($idSpectator), $comment);
            $stmt->execute();
            $id = mysqli_insert_id($this->connection);
            return parent::executeGetOne(FILM_REQUEST["film_review_ById"], $id);
        } catch (Exception $e) {
            throw new Exception("Une erreur est survenue.", $e);
        }
    }

    public function createWishlistLine($idFilm, $idSpectator, $idWistlist)
    {
        $spect = parent::executeGetOneNotClose(SPECT_REQUEST["getById"], $idSpectator);
        $film = parent::executeGetOneNotClose(FILM_REQUEST["film_Get_Id"], $idFilm);
        $wistlist = parent::executeGetOneNotClose(SPECT_REQUEST["get_wistlist_ById"], $idWistlist);

        $rqReplace = str_replace('@idWl', $idWistlist, SPECT_REQUEST["get_wistlistLine_ByFilm"]);
        $lineExist = parent::executeGetOneNotClose($rqReplace, $idFilm);
        if (isset($lineExist->id)) {
            return $lineExist;
        }

        if (! $film)
            throw new Exception("Le film n'existe pas");
        if (! $spect)
            throw new Exception("Le spectateur n'existe pas");
        if (! $wistlist)
            throw new Exception("La liste d'envie n'existe pas");

            $query = SPECT_REQUEST_CREATE_WL_LINE;
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param('ii', intval($idFilm), intval($idWistlist));
        $stmt->execute();

        $id = mysqli_insert_id($this->connection);
        $result = parent::executeGetOne(SPECT_REQUEST["get_wistlist_Line_ById"], $id);
        return $result;
    }

    function removeWishlistLine($idSpectator, $idWistlist_Line)
    {
        $spect = parent::executeGetOneNotClose(SPECT_REQUEST["getById"], $idSpectator);
        if (! $spect)
            throw new Exception("Le spectateur n'existe pas");
        $wistlist_Line = parent::executeGetOneNotClose(SPECT_REQUEST["get_wistlist_Line_ById"], $idWistlist_Line);

        if (! $wistlist_Line)
            throw new Exception("La ligne n'existe pas");
            $query = SPECT_REQUEST_DELETE_WL_LINE;
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param('i', intval($idWistlist_Line));
        $stmt->execute();
        if (false === $exec) {
            error_log('mysqli execute() failed: ');
        }
    }

    function createWishlist($idSpectator, $newWl_label)
    {
        $spect = parent::executeGetOneNotClose(SPECT_REQUEST["getById"], $idSpectator);
        if (! $spect)
            throw new Exception("Le spectateur n'existe pas");

            $query = SPECT_REQUEST_CREATE_WL;
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param('is', intval($idSpectator), $newWl_label);
        $stmt->execute();

        $id = mysqli_insert_id($this->connection);
        $result = parent::executeGetOne(SPECT_REQUEST["get_wistlist_ById"], $id);
        return $result;
        if (false === $exec) {
            error_log('mysqli execute() failed: ');
        }
    }

    function removeWishlist($idSpectator, $idWistlist)
    {
        $spect = parent::executeGetOneNotClose(SPECT_REQUEST["getById"], $idSpectator);
        if (! $spect)
            throw new Exception("Le spectateur n'existe pas");

        $wistlist = parent::executeGetOneNotClose(SPECT_REQUEST["get_wistlist_ById"], $idWistlist);

        if (! $wistlist)
            throw new Exception("La ligne n'existe pas");

            $query = SPECT_REQUEST_DELETE_WL;
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param('i', intval($idWistlist));
        $stmt->execute();
        if (false === $exec) {
            error_log('mysqli execute() failed: ');
        }
    }
}