<?php
// $request peut être
/*
 * menu
 * genre
 * country
 * film_type
 * director
 * productor
 * distributor
 * actor
 * keysearch
 */
define('FILM_REQUEST_SELECT', ' select film.*,film_type.name as film_type_name , coalesce(film_images.images_path,0) as images_path , coalesce(film_images_large.images_path,0) as  images_large_path  ');
define('FILM_REQUEST_FROM', '  from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select id from film_images where film_id=film.id and is_poster is true limit 1) left join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select id from film_images_large where film_id=film.id and is_poster is true limit 1)');
define('FILM_REQUEST_WHERE', '  where film.id in  ');
define('FILM_REQUEST_COUNT', '  select count(1) ');
define('FILM_REQUEST_WHERE_FILM', '  film.film_type_id!=12 ');
define('FILM_REQUEST_WHERE_SERIE', '  film.film_type_id=12 ');
define('FILM_REQUEST_LIMIT', '  limit 5;');
define('FILM_REQUEST_CREATE', '  insert into film (title,synopsis,updated_at) values(?,?,current_timestamp)  ');
define('FILM_REQUEST_UPDATE', '  update film set title = ?,synopsis= ? where id = ?  ');
define('FILM_REQUEST_DELETE', '  Delete from film where id = ?  ');

define('FILM_REQUEST', array(
    "getOne" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . "   where film.id = ? ",
    "suggest" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . "   where  film.id =(select max(id) from film where film_type_id = @type and id < @idFilm ) or film.id=(select min(id) from film where film_type_id = @type and id > @idFilm ) order by film.id limit @limit ",
    
    "menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =?) " . FILM_REQUEST_LIMIT,
    "genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id=?)   ",
    "country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_country join country on country.id=film_country.country_id and country.id=?) ",
    "film_type" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =? ",
    "director" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_director join director on director.id=film_director.director_id and director.id=?)  ",
    "productor" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_productor join productor on productor.id=film_productor.productor_id and productor.id=?)   ",
    "distributor" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_distributor join distributor on distributor.id=film_distributor.distributor_id and distributor.id=?)	",
    "actor" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select film_id from film_actor join actor on actor.id=film_actor.actor_id and actor.id=?)   ",
    "keyword" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "  (select id from film WHERE  MATCH(title,synopsis,original_title) AGAINST (?) > 0 )",
    /*for paging model*/  
    "all_Paging" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM ,
    /*for datatable*/  
    "all_Datatable" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM ,
    "all_Datatable_By_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE. " (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =@idParam) ",
    "all_Datatable_By_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM .  FILM_REQUEST_WHERE."(select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id=@idParam)   ",
    "all_Datatable_By_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM .  FILM_REQUEST_WHERE."(select film_id from film_country join country on country.id=film_country.country_id and country.id=@idParam) ",
    "all_Datatable_By_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM .  " where film.year_production @whereYear ",
    
    "film_Datatable" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id !=12 ",
    "film_Datatable_By_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id !=12 and film.id in " . "(select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =@idParam) ",
    "film_Datatable_By_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id !=12 and film.id in " . "(select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id=@idParam)   ",
    "film_Datatable_By_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id !=12 and film.id in " . "(select film_id from film_country join country on country.id=film_country.country_id and country.id=@idParam) ",
    "film_Datatable_By_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id !=12 and film.year_production @whereYear ",
    
    "serie_Datatable" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =12 ",
    "serie_Datatable_By_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =12 and film.id in " . "(select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =@idParam) ",
    "serie_Datatable_By_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =12 and film.id in " . "(select film_id from film_genre join genre on genre.id=film_genre.genre_id and genre.id=@idParam)   ",
    "serie_Datatable_By_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =12 and film.id in " . "(select film_id from film_country join country on country.id=film_country.country_id and country.id=@idParam) ",
    "serie_Datatable_By_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . " where film_type.id =12 and film.year_production @whereYear ",
    
    "all_keyword" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE .                 "  (select id from film WHERE  MATCH(title,synopsis,original_title) AGAINST ('@idParam') > 0 )",
    "all_serie_keyword_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .     "  (select film.id from film join film_menu on film.id=film_menu.film_id and film_menu.menu_id =@idParam    where MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "all_serie_keyword_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .    "  (select film.id from film join film_genre on film.id=film_genre.film_id and film_genre.genre_id =@idParam    where   MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "all_serie_keyword_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .  "  (select film.id from film join film_country on film.id=film_country.film_id and film_country.country_id =@idParam    where   MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "all_serie_keyword_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .    "  (select film.id from film where  film.year_production @whereYear  and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    
    
    "film_keyword" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE . "(select id from film WHERE film_type.id !=12 and MATCH(title,synopsis,original_title) AGAINST (?) > 0 )",
    "film_keyword_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE ."  (select film.id from film join film_menu on film.id=film_menu.film_id and film_menu.menu_id =@idParam    where  film_type.id !=12 and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "film_keyword_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE ."  (select film.id from film join film_genre on film.id=film_genre.film_id and film_genre.genre_id =@idParam    where film_type.id !=12 and  MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "film_keyword_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE ."  (select film.id from film join film_country on film.id=film_country.film_id and film_country.country_id =@idParam    where film_type.id !=12 and  MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "film_keyword_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE ."  (select film.id from film where  film.year_production @whereYear     and film_type.id !=12 and  MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    
    "serie_keyword" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM . FILM_REQUEST_WHERE .           "  (select id from film WHERE film_type.id =12 and MATCH(title,synopsis,original_title) AGAINST (?) > 0 )",
    "serie_keyword_menu" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .     "  (select film.id from film join film_menu on film.id=film_menu.film_id and film_menu.menu_id =@idParam    where film_type.id =12 and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "serie_keyword_genre" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .    "  (select film.id from film join film_genre on film.id=film_genre.film_id and film_genre.genre_id =@idParam    where film_type.id =12 and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "serie_keyword_country" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .  "  (select film.id from film join film_country on film.id=film_country.film_id and film_country.country_id =@idParam    where film_type.id =12 and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
    "serie_keyword_annee" => FILM_REQUEST_SELECT . FILM_REQUEST_FROM  . FILM_REQUEST_WHERE .    "  (select film.id from film where  film.year_production @whereYear  and film_type.id =12     and MATCH(film.title,film.synopsis,film.original_title) AGAINST (?) > 0 )",
  
    "all_sortMenu" => " (select 'genre', genre.id  as id, genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
            from genre join film_genre on genre.id=film_genre.genre_id join film on film.id=film_genre.film_id
            where ".FILM_REQUEST_WHERE_FILM."
            group by  1,2,3  order by genre.name )
            union
            (select 'annee', '-1930', 'origine (avant 1930)',count(id) as nbFilm
            from film
            where year_production <1930)
            union
            (select 'annee', '1930-1945', 'classique (1930-1945)',count(id) as nbFilm
            from film
            where  year_production >=1930 and year_production <=1945)
            union
            (select 'annee', '1956-1958', 'Les années 50',count(id) as nbFilm
            from film
            where  year_production >=1946 and year_production <=1958)
            union
            (select 'annee', '1959-1967', 'Les années 60',count(id) as nbFilm
            from film
            where year_production >=1959 and year_production <=1967)
            union
            (select 'annee', '1968-1983','Les années 70',count(id) as nbFilm
            from film
            where year_production >=1968 and year_production <=1983)
            union
            (select 'annee', '1984-1991','Les années 80',count(id) as nbFilm
            from film
            where  year_production >=1984 and year_production <=1991)
            union
            (select 'annee', '1992-2001','Les années 90',count(id) as nbFilm
            from film
            where year_production >=1992 and year_production <=2001)
            union
            (select 'annee', '2002-2010','Les années 2000',count(id) as nbFilm
            from film
            where year_production >=2002 and year_production <=2010)
            union
            (select 'annee', '-20072017','Les années 2010',count(id) as nbFilm
            from film
            where year_production >=2010 and year_production <=2017)
            union
            (select 'annee', '2017-', 'Après 2017',count(id) as nbFilm
            from film
            where year_production >2017)",
    
    "film_sortMenu" => " (select 'genre', genre.id  as id, genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
            from genre join film_genre on genre.id=film_genre.genre_id join film on film.id=film_genre.film_id
            where ".FILM_REQUEST_WHERE_FILM."
            group by  1,2,3 order by genre.name )
            union
            (select 'annee', '-1930', 'origine (avant 1930)',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production <1930)
            union
            (select 'annee', '1930-1945', 'classique (1930-1945)',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1930 and year_production <=1945)
            union
            (select 'annee', '1956-1958', 'Les années 50',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1946 and year_production <=1958)
            union
            (select 'annee', '1959-1967', 'Les années 60',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1959 and year_production <=1967)
            union
            (select 'annee', '1968-1983','Les années 70',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1968 and year_production <=1983)
            union
            (select 'annee', '1984-1991','Les années 80',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1984 and year_production <=1991)
            union
            (select 'annee', '1992-2001','Les années 90',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=1992 and year_production <=2001)
            union
            (select 'annee', '2002-2010','Les années 2000',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=2002 and year_production <=2010)
            union
            (select 'annee', '-20072017','Les années 2010',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >=2010 and year_production <=2017)
            union
            (select 'annee', '2017-', 'Après 2017',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_FILM." and year_production >2017)",
    "serie_sortMenu" => " (select 'genre', genre.id  as id, genre.name  as name, count(distinct(film_genre.film_id))  as nbFilm
            from genre join film_genre on genre.id=film_genre.genre_id join film on film.id=film_genre.film_id
            where ".FILM_REQUEST_WHERE_SERIE."
            group by  1,2,3  order by genre.name )
            union
            (select 'annee', '-1930', 'origine (avant 1930)',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production <1930)
            union
            (select 'annee', '1930-1945', 'classique (1930-1945)',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1930 and year_production <=1945)
            union
            (select 'annee', '1956-1958', 'Les années 50',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1946 and year_production <=1958)
            union
            (select 'annee', '1959-1967', 'Les années 60',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1959 and year_production <=1967)
            union
            (select 'annee', '1968-1983','Les années 70',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1968 and year_production <=1983)
            union
            (select 'annee', '1984-1991','Les années 80',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1984 and year_production <=1991)
            union
            (select 'annee', '1992-2001','Les années 90',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=1992 and year_production <=2001)
            union
            (select 'annee', '2002-2010','Les années 2000',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=2002 and year_production <=2010)
            union
            (select 'annee', '-20072017','Les années 2010',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >=2010 and year_production <=2017)
            union
            (select 'annee', '2017-', 'Après 2017',count(id) as nbFilm
            from film
            where ".FILM_REQUEST_WHERE_SERIE." and year_production >2017)",
  
    /*END for datatable*/ 
    
    "menuHome" => " (select 'BESTS', film.*,film_type.name as film_type_name,   coalesce(film_images.images_path,0) as images_path, coalesce(film_images_large.images_path,0) as  images_large_path
         from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select id from film_images where film_id=film.id and is_poster is true limit 1)  join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select id from film_images_large where film_id=film.id and is_poster is true limit 1)
          where  film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =1     order by updated_at desc ) limit 12)
          union
        (select 'ATCINEMA', film.*,film_type.name as film_type_name,   coalesce(film_images.images_path,0) as images_path, coalesce(film_images_large.images_path,0) as  images_large_path
         from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select id from film_images where film_id=film.id and is_poster is true limit 1) join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select id from film_images_large where film_id=film.id and is_poster is true limit 1)
          where film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =2)  order by updated_at desc limit 12) 
            union
        (select 'PREVIEW', film.*,film_type.name as film_type_name,   coalesce(film_images.images_path,0) as images_path, coalesce(film_images_large.images_path,0) as  images_large_path
         from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select id from film_images where film_id=film.id and is_poster is true limit 1)  join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select id from film_images_large where film_id=film.id and is_poster is true limit 1)
          where film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =3 order by updated_at desc ) limit 12) 
            union
        (select 'BESTSERIE', film.*,film_type.name as film_type_name,   coalesce(film_images.images_path,0) as images_path, coalesce(film_images_large.images_path,0) as  images_large_path
         from film   join film_type on film.film_type_id=film_type.id  left join  film_images on film_images.film_id=film.id and film_images.id  = (select id from film_images where film_id=film.id and is_poster is true limit 1 )  join  film_images_large on film_images_large.film_id=film.id and film_images_large.id  = (select id from film_images_large where film_id=film.id and is_poster is true limit 1)
          where film.id in (select film_id from film_menu join menu on menu.id=film_menu.menu_id and menu.id =4 order by updated_at desc ) limit 12) ",

    "actor" => "select  actor.id  as id, actor.name, role_actor.label as label , actor.idcine, actor_images.images_path as images_path,actor_images.images_large_path as images_large_path
                from actor join film_actor on actor.id=film_actor.actor_id
                left join  actor_images on actor_images.actor_id=actor.id and actor_images.id  = (select id from actor_images where actor_id=actor.id and is_poster is true limit 1)
                join film on film.id =film_actor.film_id
                left join role_actor on role_actor.id=film_actor.role_actor_id
                where film.id=?",
    "director" => "select  director.id  as id, director.name
                from director join film_director on director.id=film_director.director_id
                join film on film.id =film_director.film_id
                where film.id=?",
    "distributor" => "select  distributor.id  as id, distributor.name
                from distributor join film_distributor on distributor.id=film_distributor.distributor_id
                join film on film.id =film_distributor.film_id
                where film.id=?",
    "genre" => "select genre.id  as id, genre.name
                from genre join film_genre on genre.id=film_genre.genre_id
                join film on film.id =film_genre.film_id
                where film.id=?",
    "productor" => "select productor.id  as id, productor.name
                from productor join film_productor on productor.id=film_productor.productor_id
                join film on film.id =film_productor.film_id
                where film.id=?",
    "country" => "select country.id  as id, country.name,country.code
                from country join film_country on country.id=film_country.country_id
                join film on film.id =film_country.film_id
                where film.id=?",
    "film_images" => "select film_images.images_path
                from film_images join film on film_images.film_id=film.id
                where film.id=?",
    "film_images_large" => "select film_images_large.images_path
                from film_images_large join film on film_images_large.film_id=film.id
                where film.id=? ",
    "film_reviews" => "SELECT review.*, spectator.name,spectator.avatar FROM review  join spectator on spectator.id=review.spectator_id where review.film_id =? ",
    "film_videos" => "select film_videos.vid_med,film_videos.vid_high
                from film_videos join film on film_videos.film_id=film.id
                where film.id=? ",
    "film_Get_Id" => "SELECT id from film where id = ? ",
    "film_review_ById" => "SELECT review.*, spectator.name,spectator.avatar FROM review  join spectator on spectator.id=review.spectator_id where review.id =? ",
    
));




define('SPECT_REQUEST', array(
    "getById" => "  select * from spectator where spectator.id = ? ",
    "getByEmail" =>" select * from spectator where spectator.email = ? ",
    "getWishList_BySpect" =>" select * from wishlist where spectator_id =? ",
    "get_wistlist_ById" =>" select * from wishlist where id =? ",
    "get_wistlist_Line_ById" =>" select wishlist_line.*, wishlist.label as label from wishlist_line join wishlist on wishlist.id=wishlist_line.wishlist_id where wishlist_line.id =? ",
    "get_wistlistLine_ByFilm" =>" select * from wishlist_line where film_id =? and wishlist_id =@idWl limit 1 ",
    "getWishListLine_ByWishList" =>FILM_REQUEST_SELECT ." ,wishlist_line.id as wishlist_line_id " . FILM_REQUEST_FROM . " join wishlist_line on film.id=wishlist_line.film_id where  wishlist_line.wishlist_id =? "
));
define('SPECT_REQUEST_CREATE_REVIEW', '  insert into review (film_id,spectator_id,comments,updated_at) values(?,?,?,current_timestamp)  ');
define('SPECT_REQUEST_CREATE_WL_LINE', ' insert into wishlist_line (film_id,wishlist_id,updated_at) values(?,?,current_timestamp) ');
define('SPECT_REQUEST_DELETE_WL_LINE', '  delete from wishlist_line where id =?  ');
define('SPECT_REQUEST_CREATE_WL', ' insert into wishlist (spectator_id,label,updated_at) values(?,?,current_timestamp) ');
define('SPECT_REQUEST_DELETE_WL', ' delete from wishlist where id =? ');
define('SPECT_REQUEST_CREATE', ' insert into spectator (name,email,password,created_at, updated_at) values(?,?,?,current_timestamp,current_timestamp) ');
define('SPECT_REQUEST_UPDATE', ' update spectator set name = ?,email= ?, password= ? where id = ? ');
define('SPECT_REQUEST_DELETE', ' Delete from spectator where id = ? ');
define('SPECT_REQUEST_LOGIN', ' select * from spectator where email = ? ');
define('SPECT_REQUEST_SELECT_BYID', ' select * from spectator where id = ? ');

?>

