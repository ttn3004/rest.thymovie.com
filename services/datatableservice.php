<?php
include_once __DIR__ . '/../config/dbclass.php';
require_once __DIR__ . '/../config/meta.php';
require_once 'sql.php';
require_once 'iservice.php';
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

// remove all warning
class DataTableService extends IService
{

    public $columns = array(
        0 => 'id',
        1 => 'title',
        2 => 'synopsis',
        3 => 'duration',
        4 => 'updated_at'
    );

    public $params = array();

    public $sql_query = "";

    public $sqlTot = "";

    public $sqlRec = "";

    function __construct($params)
    {
        $this->params = $params;
        parent::__construct();
        $posLastWhere = 0;
        $posLastParenthesis = 99999;
        if (! empty($params['search']['value'])) {
            
            if (! empty($params['name']) && ! empty($params['param'])) {
                if($params['name'] =='annee' ){
                    $strDate=$params['param'];
                    if(strpos($strDate, '-')===0){
                        $this->sql_query = str_replace('@whereYear', ' < '. substr ($params['param'],1,strlen($params['param'])), FILM_REQUEST[$params['filmType']. "_keyword_" . $params['name']]);
                    }else  if(strpos($strDate, '-')===strlen($strDate)-1){
                        $this->sql_query = str_replace('@whereYear', ' > '. substr ($params['param'],0,strlen($params['param'])-1), FILM_REQUEST[$params['filmType']."_keyword_" . $params['name']]);
                    }else{
                        $dateArr=explode("-", $strDate);
                        $this->sql_query = str_replace('@whereYear', ' between '.$dateArr[0].' AND '.$dateArr[1] , FILM_REQUEST[$params['filmType']."_keyword_" . $params['name']]);
                    }
                }else{
                    $searchKeyExplode=explode('-', $params['param']);
                    $this->sql_query = str_replace('@idParam', implode(' ', $searchKeyExplode), FILM_REQUEST[$params['filmType']."_keyword_" . $params['name']]);
                }
            } else {
                $this->sql_query = FILM_REQUEST[$params['filmType']."_keyword"];
            }
            $posLastParenthesis = strripos($this->sql_query, ')', 1)-1;
        } else {

            if  (isset($params['name']) && isset($params['param'])) {
                if($params['name'] =='annee' ){
                    $strDate=$params['param'];
                    if(strpos($strDate, '-')===0){
                        $this->sql_query = str_replace('@whereYear', ' < '. substr ($params['param'],1,strlen($params['param'])), FILM_REQUEST[$params['filmType']."_Datatable_By_" . $params['name']]);
                    }else  if(strpos($strDate, '-')===strlen($strDate)-1){
                        $this->sql_query = str_replace('@whereYear', ' > '. substr ($params['param'],0,strlen($params['param'])-1), FILM_REQUEST[$params['filmType']."_Datatable_By_" . $params['name']]);
                    }else{
                        $dateArr=explode("-", $strDate);
                        $this->sql_query = str_replace('@whereYear', ' between '.$dateArr[0].' AND '.$dateArr[1] , FILM_REQUEST["film_Datatable_By_" . $params['name']]);
                    }
                }else{
                    $searchKeyExplode=explode('-', $params['param']);
                    $this->sql_query = str_replace('@idParam',implode(' ', $searchKeyExplode), FILM_REQUEST[$params['filmType']."_Datatable_By_" . $params['name']]);
                }
                
            } else if  (isset($params['param'])) {
                $searchKeyExplode=explode('-', $params['param']);
                $this->sql_query = str_replace('@idParam',implode(' ', $searchKeyExplode), FILM_REQUEST[$params['filmType']."_keyword"]);
                
                $posLastParenthesis = strripos($this->sql_query, ')', 1)-1;
            }  else {
                $this->sql_query = FILM_REQUEST[$params['filmType']."_Datatable"];
            }
        }
        
        $posLastWhere = strripos($this->sql_query, 'where', 1);
        $longToSub=$posLastParenthesis-$posLastWhere;
        $this->sqlTot .=" select count(1) from film join film_type on film.film_type_id=film_type.id  " . substr($this->sql_query , $posLastWhere, $longToSub);//strlen($this->sql_query)
        
//         var_dump($this->sql_query);
//         var_dump($this->sqlTot);
        
        $this->sqlRec .= $this->sql_query;
        $this->sqlRec .= " order by   updated_at DESC " . "  LIMIT " . $params['start'] . " ," . $params['length'] . " ";
    }

    public function get()
    {
        if (strpos($this->sqlTot, '(?)')) {
            $this->sqlTot = str_replace("?", "'" . $this->params['search']['value'] . "'", $this->sqlTot);
            $this->sqlRec = str_replace("?", "'" . $this->params['search']['value'] . "'", $this->sqlRec);
        }
        
        $totalRows = mysqli_query($this->connection, $this->sqlTot) or die("Database Error:" . mysqli_error($this->connection));
        $row = $totalRows->fetch_row();
        $totalRecords =$row[0];
        
        $queryRecords = mysqli_query($this->connection, $this->sqlRec) or die("Error to Get the Post details.");
        while ($row = mysqli_fetch_row($queryRecords)) {
            $this->data[] = $row;
        }
        if($this->data==null)
            $this->data=array();
        return array(
            "draw" => intval($this->params['draw']),
            "recordsTotal" => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data" => $this->data
        );
    }
}
?>