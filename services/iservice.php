<?php

class IService
{

    // Connection instance
    public $connection;

    public function __construct()
    {
        $dbclass = new DBClass();
        $this->connection = $dbclass->getConnection();
    }

    public function bind_result_array($stmt)
    {
        $meta = $stmt->result_metadata();
        $result = array();
        while ($field = $meta->fetch_field()) {
            $result[strtolower($field->name)] = NULL;
            $params[] = &$result[strtolower($field->name)];
        }

        call_user_func_array(array(
            $stmt,
            'bind_result'
        ), $params);
        return $result;
    }

    /**
     * Returns a copy of an array of references
     */
    public function getCopy($row)
    {
        return array_map(
            // create_function('$a', 'return $a;'), $row
            function ($a) {
                return $a;
            }, $row);
    }

    public function executeGetOne($query, $id)
    {
        $stmt = $this->connection->prepare($query);
        if (is_numeric($id))
            $stmt->bind_param('i', $id);
        else
            $stmt->bind_param('s', $id);
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows();

        if ($count > 0) {
            $row = $this->bind_result_array($stmt);
            /* fetch values */
            $film = null;
            if ($stmt->fetch()) {
                $film = $this->getCopy($row);
            }
            $stmt->close();
            $this->connection->close();
            return $film; // Parse to JSON and print.
        } else {
            $stmt->close();
            $this->connection->close();
            return json_decode("{}");
        }
    }

    public function executeGet($query, $parameter)
    {
        if ($stmt = $this->connection->prepare($query)) {
            if (isset($parameter)) {
                if (is_numeric($parameter))
                    $stmt->bind_param('i', $parameter);
                else
                    $stmt->bind_param('s', $parameter);
            }

            $stmt->execute();
            $stmt->store_result();
            $count = $stmt->num_rows();
            if ($count > 0) {
                $row = $this->bind_result_array($stmt);
                $film_arr = array();
                while ($stmt->fetch()) {
                    array_push($film_arr, $this->getCopy($row));
                }
                $stmt->close();
                $this->connection->close();
                return $film_arr; // Parse to JSON and print.
            } else {
                $stmt->close();
                $this->connection->close();
                return array();
            }
        } else {
            $this->connection->close();
            return array();
        }
    }

    
    public function executeGetOneNotClose($query, $id)
    {
        $stmt = $this->connection->prepare($query);
        if (is_numeric($id))
            $stmt->bind_param('i', $id);
        else
            $stmt->bind_param('s', $id);

        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows();

        if ($count > 0) {
            $row = $this->bind_result_array($stmt);
            /* fetch values */
            $film = null;
            if ($stmt->fetch()) {
                $film = $this->getCopy($row);
            }
            $stmt->close();
            return $film; // Parse to JSON and print.
        } else {
            $stmt->close();
            return json_decode("{}");
        }
    }

    public function executeGetNotClose($query, $parameter)
    {
        if ($stmt = $this->connection->prepare($query)) {
            if (isset($parameter)) {
                if (is_numeric($parameter))
                    $stmt->bind_param('i', $parameter);
                else
                    $stmt->bind_param('s', $parameter);
            }

            $stmt->execute();
            $stmt->store_result();
            $count = $stmt->num_rows();
            if ($count > 0) {
                $row = $this->bind_result_array($stmt);
                $film_arr = array();
                while ($stmt->fetch()) {
                    array_push($film_arr, $this->getCopy($row));
                }
                $stmt->close();
                return $film_arr; // Parse to JSON and print.
            } else {
                $stmt->close();
                return array();
            }
        } else {
            return array();
        }
    }
    
    public function executeRq($query){
        if ($stmt = $this->connection->prepare($query)) {
            $stmt->execute();
            $stmt->store_result();
            $count = $stmt->num_rows();
            if ($count > 0) {
                $row = $this->bind_result_array($stmt);
                $film_arr = array();
                while ($stmt->fetch()) {
                    array_push($film_arr, $this->getCopy($row));
                }
                $stmt->close();
                $this->connection->close();
                return $film_arr; // Parse to JSON and print.
            } else {
                $stmt->close();
                $this->connection->close();
                return array();
            }
        } else {
            $this->connection->close();
            return array();
        }
    }
}

?>